/* this algorithm is written by keeping in mind that,
all the rounds/timeslots, mentioning companies and student they are interviewing,
are to be determined before start of interview process
*/

const CP = {
	C1: ['S2', 'S1', 'S3'],
	C2: ['S3', 'S2', 'S1'],
	C3: ['S2', 'S3', 'S1']
} /* object containing key value pairs, where key is company code[C1, C2 ... Cn], and its value is array of students/candidates, with decresing order of preference, 
e-g - CP = {C1: [S4, S2, S9, S0..............], C1: [S0, S9 .......]} */

const SP = {
	S1: ['C2', 'C3', 'C1'],
	S2: ['C3', 'C2', 'C1'],
	S3: ['C1', 'C2', 'C3']
} /* object containing key value pairs, where key is student's code,[S1, S2 .... Sn] and its value is array of companies, with decresing order of preference,
e-g - SP = {S1: [C3, C0, C7, C4..............], S2: [C3 .....]} */

const interviews = {
	// object keeping record off interviews that have been fixed
	T1: {},
	T2: {},
	T3: {}
}

const engagements = {
	// object showing companies, students have interviews with in particular round
	S1: {},
	S2: {},
	S3: {}
}

// ranking students based on the number of interview call, a student got, and his/her preference rank, made by company
const studentRankingObj = {} 
const companyRankingObj = {} 
/* (student/company)RankingObject consisting scores of each student/company in key value pair
e.g. - {
			S1: {
					student: 'S1',
					score: 53
			},
			S2: {
				student: 'S2',
				score: 47
			},
			S3 .......
		}

		higher score signifies, highly liked student/company
*/ 

Object.keys(CP).forEach((companyCode) => {

	const companyPreferences = CP[companyCode]

	/*looping through array consisting students in decreasing order of preference,
	for lower value of n, or higher preference, score will be high */

	companyPreferences.forEach((student, n) => {

		const score = companyPreferences.length - n // where n is the index in the array

		if (studentRankingObj.hasOwnProperty(student)) {
			studentRankingObj[student].score += score
		} else {
			studentRankingObj[student] = {student, score}
		}

	})
})

Object.keys(SP).forEach((studentCode) => {

	const studentPreferences = SP[studentCode]

	/*looping through arrays consisting companies in decreasing order of preference, for a particular student.
	For lower value of n (index), or higher preference, score will be high */

	studentPreferences.forEach((company, n) => {

		const index = CP[company].indexOf(studentCode)

		index === -1 ? SP[studentCode].splice(index, 1) : ''// remove company from preference list of students, if student in not in company's preference list

		const score = studentPreferences.length - n // where n is the index in the array

		if (companyRankingObj.hasOwnProperty(company)) {
			companyRankingObj[company].score += score
		} else {
			companyRankingObj[company] = {company, score}
		}

	})
})

let studentRanking = Object.values(studentRankingObj)
let companyRanking = Object.values(companyRankingObj)

studentRanking = studentRanking.sort((a, b) => {
	return b.score - a.score
}) // returns array of objects {student, score}, in decreasing order of score

companyRanking = companyRanking.sort((a, b) => {
	return b.score - a.score
}) // returns array of objects {company, score}, in decreasing order of score

// number of timeslots === s (number of shortlists per company)
/*
interviews[Object] ==>  which in future will contain, timeslots as keys, aka- T1, T2, T3
and corresponding values are object consisting of company-student pairs
eg - {T1: {C3: 'S4', C1: 'S6' ......}, T2: {C3: 'S1'} .............Tz}
*/


function checkSlot (studentRank, round) { // check if any timeslot is empty for top preferred company of top ranked student

	if (round > 3) {
		if (studentRank === studentRanking.length -1 && round > 3) {
			finish()
			return
		} else {
			round = 1
		}
	}

	

	const student = studentRanking[studentRank].student

	const company = SP[student][0]

	if (!interviews['T' + round][company] && !engagements[student]['T' + round]) {
		// timeslot is partially filled & preffered company of student has not been assigned any student
		bookSlot(studentRank, round, company)
	} else {

		// preffered company has been booked for this slot. Check same company for next round

		checkSlot(studentRank, round+1)
	}
}

const slotTable = {
	T1: ['C1', 'C2', 'C3'],
	T2: ['C1', 'C2', 'C3'],
	T3: ['C1', 'C2', 'C3']
}

function bookSlot (studentRank, round, company) {

	const student = studentRanking[studentRank].student
	SP[student].splice(SP[student].indexOf(company), 1) // removing this company from student's preference

	interviews['T' + round][company] = student // assigning particular timeslot for particular company to student
	
	if (slotTable['T' + round]) {
		const index = slotTable['T' + round].indexOf(company)
	
		slotTable['T' + round].splice(index, 1)
		
		if (slotTable['T' + round].length === 0) {
			delete slotTable['T' + round]
		}
	}

	if (!engagements[student]['T' + round]) {
		engagements[student]['T' + round] = company
	}

	// console.log(interviews)

	if (SP[student].length === 0) {
		// interview with all companies for this student have been fixed
		delete SP[student]
		checkSlot(++studentRank, ++round) // check slots & companies in next round for next lower student
	} else {
		checkSlot(studentRank, ++round) // check slots & companies in next round for same student
	}
}

function finish () {
	Object.keys(slotTable).forEach((slot) => {
		const companies = slotTable[slot]
		companies.forEach((company) => {
			Object.keys(SP).forEach((student) => {
				if ((!engagements[student][slot]) && SP[student].indexOf(company) > -1) {
					interviews[slot][company] = student
				}
			})
		})
	})
	console.log(interviews)
}

// start checking slots for students, top-side-down, starting with top ranked student, with his top preffered company, for 1st round/timeslot aka 'T1'
checkSlot(0, 1) // arg1 ==> index of student in studentRanking array, arg2 ==> round/timeslot to start with